FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm install ngx-bootstrap --save
RUN npm install bootstrap@3.3.7
RUN npm add ngx-bootstrap  --component carousel
RUN $(npm bin)/ng build

# stage 2
FROM nginx:alpine
COPY --from=node /app/dist/myproject /usr/share/nginx/html
