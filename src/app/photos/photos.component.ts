import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.less']
})
export class PhotosComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit() {
  }
message(){
	this.router.navigate(['memory']);
}
}
